package com.homework.lab4.stopwatch;

import android.os.Build;
import android.os.Handler;
import android.support.annotation.RequiresApi;

import java.util.function.Consumer;

public class SecondTimer implements Timer {

    private int seconds = 0;
    private boolean isRunning;

    @Override
    public boolean isRunning() {
        return isRunning;
    }

    @Override
    public int getSeconds() {
        return seconds;
    }

    @Override
    public void setSeconds(int seconds) {
        this.seconds = seconds;
    }

    @Override
    public void start() {
        isRunning = true;
    }

    @Override
    public void stop() {
        isRunning = false;
    }

    @Override
    public void reset() {
        seconds = 0;
    }

    @Override
    public void runTimer(Consumer<String> funcRunWith) {
        final Handler timeHandler = new Handler();
        timeHandler.post(new Runnable() {
            @RequiresApi(api = Build.VERSION_CODES.N)
            @Override
            public void run() {
                int hours = seconds / 3600;
                int minutes = (seconds % 3600) / 60;
                int secs = seconds % 60;
                String time = String.format("%d:%02d:%02d", hours, minutes, secs);
                funcRunWith.accept(time);
                if (isRunning) {
                    seconds++;
                }
                timeHandler.postDelayed(this, 1000);
            }
        });
    }
}
