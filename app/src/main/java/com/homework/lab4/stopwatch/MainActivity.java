package com.homework.lab4.stopwatch;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.widget.Button;
import android.widget.TextView;

public class MainActivity extends AppCompatActivity {

    private Timer secondTimer;
    private boolean wasRunning;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        initTimer(savedInstanceState);
        initListeners();
        drawTime();
    }

    @Override
    protected void onStart() {
        super.onStart();
        if(wasRunning) {
            secondTimer.start();
        }
    }

    @Override
    protected void onStop() {
        super.onStop();
        wasRunning = secondTimer.isRunning();
        secondTimer.stop();
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        outState.putInt("seconds", secondTimer.getSeconds());
        outState.putBoolean("isRunning", secondTimer.isRunning());
    }

    private void initTimer(Bundle savedInstanceState) {
        secondTimer = new SecondTimer();
        if (savedInstanceState != null) {
            secondTimer.setSeconds(savedInstanceState.getInt("seconds"));
            if (savedInstanceState.getBoolean("isRunning")) {
                secondTimer.start();
            } else {
                secondTimer.stop();
            }
        }
    }

    private void drawTime() {
        final TextView timeView = findViewById(R.id.textView);
        secondTimer.runTimer(timeView::setText);
    }

    private void initListeners() {
        Button resetButton = findViewById(R.id.button);
        Button startButton = findViewById(R.id.button3);
        Button stopButton = findViewById(R.id.button2);

        resetButton.setOnClickListener(
                button -> {
                    secondTimer.stop();
                    secondTimer.reset();
                });

        startButton.setOnClickListener(button -> secondTimer.start());
        stopButton.setOnClickListener(button -> secondTimer.stop());
    }
}
