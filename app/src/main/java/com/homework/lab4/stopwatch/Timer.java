package com.homework.lab4.stopwatch;

import java.util.function.Consumer;

public interface Timer {
    void start();
    void stop();
    void reset();
    void runTimer(Consumer<String> funcRunWith);
    boolean isRunning();
    int getSeconds();
    void setSeconds(int seconds);
}
