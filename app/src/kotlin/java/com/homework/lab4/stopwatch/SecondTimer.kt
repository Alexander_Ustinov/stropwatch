package com.homework.lab4.stopwatch

import android.os.Build
import android.os.Handler
import android.support.annotation.RequiresApi

class SecondTimer : Timer {

    private var seconds = 0;
    private var isRunning = false;

    override fun start() {
        isRunning = true
    }

    override fun stop() {
        isRunning = false
    }

    override fun reset() {
        seconds = 0
    }

    override fun runTimer(consumer: (String) -> Unit) {
        val timeHandler = Handler()
        timeHandler.post {
            object : Runnable {
                @RequiresApi(Build.VERSION_CODES.N)
                override fun run() {
                    val hours = seconds / 3600
                    val minutes = (seconds % 3600) / 60
                    val secs = seconds % 60
                    val time = String.format("%d:%02d:%02d", hours, minutes, secs)
                    consumer(time)
                    if (isRunning) {
                        seconds++
                    }
                    timeHandler.postDelayed(this, 1000)
                }
            }
        }
    }

    override fun isRunning(): Boolean {
        return isRunning
    }

    override fun getSeconds(): Int {
        return seconds
    }

    override fun setSeconds(seconds: Int) {
        this.seconds = seconds
    }
}