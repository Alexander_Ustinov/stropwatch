package com.homework.lab4.stopwatch

import android.support.v7.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.TextView

class MainActivity : AppCompatActivity() {

    private var wasRunning: Boolean = false
    private lateinit var secondTimer: Timer

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        initTimer(savedInstanceState)
        initListeners()
        drawTime()
    }

    override fun onStart() {
        super.onStart()
        if (wasRunning) {
            secondTimer.start()
        }
    }

    override fun onStop() {
        super.onStop()
        wasRunning = secondTimer.isRunning()
        secondTimer.stop()
    }

    override fun onSaveInstanceState(outState: Bundle?) {
        super.onSaveInstanceState(outState)
        outState!!.putInt("seconds", secondTimer.getSeconds())
        outState.putBoolean("isRunning", secondTimer.isRunning())
    }

    private fun initTimer(savedInstanceState: Bundle?) {
        secondTimer = SecondTimer()

        if (savedInstanceState != null) {
            secondTimer.setSeconds(savedInstanceState.getInt("seconds"))
            if (savedInstanceState.getBoolean("isRunning")) {
                secondTimer.start()
            } else {
                secondTimer.stop()
            }
        }
    }

    private fun drawTime() {
        val timeTextView = findViewById<TextView>(R.id.textView)
        secondTimer.runTimer(timeTextView::setText)
    }

    private fun initListeners() {
        val resetButton = findViewById<Button>(R.id.button)
        val startButton = findViewById<Button>(R.id.button3)
        val stopButton = findViewById<Button>(R.id.button2)

        resetButton.setOnClickListener {
            secondTimer.stop()
            secondTimer.reset()
        }

        startButton.setOnClickListener { secondTimer.start() }
        stopButton.setOnClickListener { secondTimer.stop() }
    }
}
