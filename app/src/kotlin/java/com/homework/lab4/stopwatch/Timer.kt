package com.homework.lab4.stopwatch

interface Timer {
    fun start()
    fun stop()
    fun reset()
    fun runTimer(consumer: (String) -> Unit)
    fun isRunning(): Boolean
    fun getSeconds(): Int
    fun setSeconds(seconds: Int)
}